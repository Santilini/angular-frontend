import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostsComponent } from './components/posts/posts.component';
import { RouterModule, Routes } from '@angular/router';
import { NewpostComponent } from './components/newpost/newpost.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { EditpostComponent } from './components/editpost/editpost.component';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2'
import { MomentModule } from 'ngx-moment';
import { PostComponent } from './components/post/post.component';
import { ReplyComponent } from './components/reply/reply.component';
import { ReplylistComponent } from './components/replylist/replylist.component';
import { NewreplyComponent } from './components/newreply/newreply.component';

const routes : Routes = [
  { path: 'posts', component: PostsComponent},
  { path: 'new', component: NewpostComponent},
  { path: 'edit/:id', component: EditpostComponent},
  { path: '', 
    redirectTo: '/posts',
    pathMatch: 'full'}]
@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    NewpostComponent,
    EditpostComponent,
    PostComponent,
    ReplyComponent,
    ReplylistComponent,
    NewreplyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {enableTracing: true}),
    SweetAlert2Module,
    FormsModule,ReactiveFormsModule,
    MomentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
