import { HttpClient } from '@angular/common/http'
import {baseUrlRequest} from './urls'
import {Post} from '../interfaces/post'
import { Observable } from 'rxjs'
export class PostUrlRequest extends baseUrlRequest {
  private route : string

  constructor(httpClient: HttpClient) {
    super(httpClient)
    super.setJsonHeaders()
    this.route = 'posts/'
  }
  public getAllPosts() {
    return this.getAll<Post>(this.route)
  }
  public getPostById(id: string) {
    return this.getById<Post>(this.route, id)
  }
  public CreatePost(post: Post) {
    return this.create<Post>(this.route, post)
  }
  public EditPost(id: string, post: Post) {
    return this.edit<Post>(this.route + id, post)
  }
  public DeletePost(id: string) : Observable<Post> {
    return this.deleteById<Post>(this.route, id)
  }

}