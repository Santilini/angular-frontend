import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http'
import { catchError } from 'rxjs/operators'
import { Observable, throwError } from 'rxjs'
import {Post} from '../interfaces/post'

export abstract class baseUrlRequest {
  private SERVERURL : string = 'http://post-challenge.test/api/'
  private httpOptions 
  private httpClient: HttpClient
  constructor(http: HttpClient) {
    this.httpClient = http
  }
  
  handlerError(error : HttpErrorResponse) {
    if ( error.error instanceof Error) {
      console.log('error de front' + error.error.message)
    } else {
      console.log('error de back end: ' + error.status)
      console.log('Cuerpo del error ' + error.message)
    }
    return throwError('Error de comunicacion HTTP')
    
  }
  protected getAll<T>(resource: string): Observable<Array<T>> {
    return this.httpClient.get<T[]>(this.SERVERURL + resource).pipe(catchError(this.handlerError))
  }
  protected getById<T>(resource: string, id: string): Observable<T> {
    return this.httpClient.get<T>(this.SERVERURL + resource + id).pipe(catchError(this.handlerError))
  }
  protected create<T>(resource: string, body: T) {
    return this.httpClient.post<T>(this.SERVERURL + resource, body, this.httpOptions).pipe(catchError(this.handlerError))
  }
  protected edit<T>(resource: string, body: T) {
    return this.httpClient.put<T>(this.SERVERURL + resource, body, this.httpOptions).pipe(catchError(this.handlerError))
  }
  protected deleteById<T>(resource: string, id: string): Observable<T>{
    return this.httpClient.delete<T>(this.SERVERURL + resource + id).pipe(catchError(this.handlerError))
  } 
  protected objectToHttpParams(obj: object): HttpParams {
    let params = new HttpParams();
    for (const key of Object.keys(obj)) {
      params = params.set(key, (obj[key] as unknown) as string);
    }

    return params;
  }
  public setJsonHeaders () {
    this.httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": 'application/json'
      })
    }
  }
}