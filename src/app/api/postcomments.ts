import { HttpClient } from '@angular/common/http'
import {baseUrlRequest} from './urls'
import {Comment} from '../interfaces/comment'
import { Observable } from 'rxjs'
export class PostCommentRequest extends baseUrlRequest {
  private route : string
  private subRoute: string
  constructor(httpClient: HttpClient) {
    super(httpClient)
    super.setJsonHeaders()
    this.route = 'posts/'
    this.subRoute = '/comments/'
  }
  public getAllCommentsOfPost(postId: number) {
    return this.getAll<Comment>(this.route + postId + this.subRoute)
  }
  // public getPostById(id: string) {
  //   return this.getById<Post>(this.route, id)
  // }
  public CreateCommentOfPost(comment: Comment, postId: number) {
    return this.create<Comment>(this.route + postId + this.subRoute, comment)
  }
  // public EditPost(id: string, post: Post) {
  //   return this.edit<Post>(this.route + id, post)
  // }
  public DeleteCommentFromPost(postId: string, commentId) : Observable<Comment> {
    return this.deleteById<Comment>(this.route + postId + this.subRoute, commentId)
  }

}