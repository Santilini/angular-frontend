import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {PostUrlRequest} from '../api/post'
import {Post} from '../interfaces/post'
import {PostCommentRequest} from '../api/postcomments'
import { Comment } from '../interfaces/comment';
@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private request: PostUrlRequest
  private commentsRequest: PostCommentRequest
  constructor(httpclient: HttpClient) {
    this.request = new PostUrlRequest(httpclient)
    this.commentsRequest = new PostCommentRequest(httpclient)
  }
  public getPosts() {
    return this.request.getAllPosts()
  }
  public getPostById(id: string) {
    return this.request.getPostById(id)
  }
  public createPost(body: Post) {
    return this.request.CreatePost(body)
  }
  public editPost(id: string, body: Post) {
    return this.request.EditPost(id, body)
  }
  public deletePost(id: string) {
    return this.request.DeletePost(id)
  }
  public commentPost(body: Comment, postId : number) {
    return this.commentsRequest.CreateCommentOfPost(body, postId)
  }
  public getPostComments(postId) {
    return this.commentsRequest.getAllCommentsOfPost(postId)
  }
  public deletePostComment(postId, commentId) {
    return this.commentsRequest.DeleteCommentFromPost(postId, commentId)
  }
}

