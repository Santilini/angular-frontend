import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PostsService } from 'src/app/services/posts.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-newpost',
  templateUrl: './newpost.component.html',
  styleUrls: ['./newpost.component.css']
})
export class NewpostComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private postSerice: PostsService, private router: Router) {
    this.form = formBuilder.group({
      name: ['', Validators.compose([
        Validators.required
      ])],
      content: ['', Validators.compose([
        Validators.required
      ])],
      image: ['', Validators.compose([
        Validators.required
      ])],
      user_id: 1,
    })
  }
  form : FormGroup
  invalidFields : boolean = false

  ngOnInit(): void {
  }
  createPost() {
    console.log(this.form.value)
    this.postSerice.createPost(this.form.value).subscribe(obj => {
      console.log('se creo el post')
      Swal.fire('Creación exitosa').then(() => { this.router.navigateByUrl('/posts')

      })
    })
  }
}
