import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Post} from 'src/app/interfaces/post'
import Swal from 'sweetalert2';
import {PostsService} from '../../services/posts.service'
import {PostComponent} from '../post/post.component'
import {ReplylistComponent} from '../replylist/replylist.component'
// import {moment} from 'ngx-moment'
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  template: `Last updated: {{myDate | amDateFormat:'LL'}}`
})
export class PostsComponent implements OnInit {
    posts: Post[] = []
    constructor(private postService: PostsService, activatedRoute: ActivatedRoute) {
    activatedRoute.params.subscribe(params => {
      this.id = params['id']
    })
  }
  id: string
  ngOnInit(): void {
    this.getAllPosts()
  }
  getAllPosts() {
    this.postService.getPosts().subscribe((posts : Post[]) => {
      this.posts = posts
    })
  }
  deletePost(id) {
    console.log(id)
    this.postService.deletePost(id).subscribe(() => {
      Swal.fire('El Post ha sido borrado').then(() => {
        this.getAllPosts()
      })
    })
  }
}
