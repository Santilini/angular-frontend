import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewreplyComponent } from './newreply.component';

describe('NewreplyComponent', () => {
  let component: NewreplyComponent;
  let fixture: ComponentFixture<NewreplyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewreplyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewreplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
