import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostsService } from 'src/app/services/posts.service';
import Swal from 'sweetalert2';
import {PostCommentRequest} from '../../api/postcomments'
@Component({
  selector: 'app-newreply',
  templateUrl: './newreply.component.html',
  styleUrls: ['./newreply.component.css']
})
export class NewreplyComponent implements OnInit {
  @Input() post_id: number
  @Output() onUpdate: EventEmitter<any> = new EventEmitter()
  constructor(private formBuilder: FormBuilder, private postService : PostsService) {
    console.log(this.post_id)
    this.form = formBuilder.group({
      content: ['', Validators.compose([
        Validators.required
      ])]
    })
    
  }
  form : FormGroup
  invalidFields : boolean = false

  ngOnInit(): void {
  }
  reply() {
    console.log(this.post_id)
    console.log(this.form.value)
    let comment = {
      content: this.form.value.content,
      post_id: this.post_id,
      id: 0,
      created_at: new Date()
    }
    this.postService.commentPost(comment, this.post_id).subscribe(o => {
      Swal.fire('Se ha comentado con éxito')
      this.onUpdate.emit('')

    })
  }
}
