import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Post } from 'src/app/interfaces/post';
import { PostsService } from 'src/app/services/posts.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editpost',
  templateUrl: './editpost.component.html',
  styleUrls: ['./editpost.component.css']
})
export class EditpostComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private postService: PostsService, private route: ActivatedRoute, private router: Router) {
    this.form = formBuilder.group({
      name: ['', Validators.compose([
        Validators.required
      ])],
      content: ['', Validators.compose([
        Validators.required
      ])],
      image: ['', Validators.compose([
        Validators.required
      ])],
      user_id: 1,
    })
  }
  form : FormGroup
  invalidFields : boolean = false
  id: string
  post: Post
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id']
      this.postService.getPostById(this.id).subscribe(response => {
        this.post = response
        console.log(this.post)
        this.form.patchValue({
          name: this.post.name,
          content: this.post.content,
          image: this.post.image,
          user_id: this.post.user_id
        })
      } )
    })
    // this.postService.getPostById
  }

  editPost() {
    this.postService.editPost(this.id, this.form.value).subscribe((answer) => {
      Swal.fire('Edición exitosa').then(() => { this.router.navigateByUrl('/posts')

      })
      this.router.navigateByUrl('/posts')
    })
  }

}
