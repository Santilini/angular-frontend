import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from 'src/app/interfaces/comment';
import {Post} from 'src/app/interfaces/post'
import Swal from 'sweetalert2';
import {PostsService} from '../../services/posts.service'
import {NewreplyComponent} from '../newreply/newreply.component'
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  template: `Last updated: {{myDate | amDateFormat:'LL'}}`
})
export class PostComponent implements OnInit {

  @Input() post: Post
  @Output() onDelete: EventEmitter<any> = new EventEmitter()
  comments: Comment[] = []
  isCreatingNewReply = false
  areCommentsOpen = false
  
  constructor(private postService : PostsService) {
    
  }

  ngOnInit(): void {
    
  }

  deletePost(id) {
    console.log(id)
    this.onDelete.emit(id)    
  }
  getCommentsText() {
    if (this.post.comment_count == 0) {
      return 'Sin comentarios'
    }
    let text = `${this.post.comment_count} comentario`
    if (this.post.comment_count > 1) {
      text = text + 's' 
    }
    return text
  }
  changeReplyCreationBar() {
    this.isCreatingNewReply = !this.isCreatingNewReply
  }
  updateComments() {
    this.postService.getPostComments(this.post.id).subscribe((comments : Comment[]) => {
      this.comments = comments
      console.log(comments)
    })
  }
  onDonePosting() {
    this.changeReplyCreationBar()
    this.updateComments()
  }
  manageCommentList() {
    if (!this.areCommentsOpen) {
      this.areCommentsOpen = true
      this.updateComments()
    } else {
      this.areCommentsOpen = false
    }
  }
  deleteComment(id) {
    console.log(id)
    this.postService.deletePostComment(this.post.id, id).subscribe( o => {
      this.updateComments()
    })
  }
}

