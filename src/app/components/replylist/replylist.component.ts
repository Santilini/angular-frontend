import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {Comment} from '../../interfaces/comment'
@Component({
  selector: 'app-replylist',
  templateUrl: './replylist.component.html',
  styleUrls: ['./replylist.component.css']
})
export class ReplylistComponent implements OnInit {
  @Input () comments: Comment[]
  @Output() onDelete: EventEmitter<any> = new EventEmitter()
  constructor() { 

  }

  ngOnInit(): void {
  }

  deleteMe(id) {
    console.log(id)
    this.onDelete.emit(id)
  }
}
