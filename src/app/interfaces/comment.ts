export interface Comment {
  id: number,
  created_at: Date,
  content: String,
  post_id: number
}