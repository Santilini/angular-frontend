export interface Post {
  id: Number
  name: String
  content: String
  image: String
  user_id: Number
  created_at: Date
  comment_count: Number
}